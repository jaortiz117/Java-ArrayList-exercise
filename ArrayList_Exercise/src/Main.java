import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Main {
	
	static ArrayList<Object> list = new ArrayList<>(0);

	public static void main(String[] args) {
		System.out.println("Enter request:");
		while(true) {
			System.out.println("");
			// create a scanner so we can read the command-line input
			Scanner scanner = new Scanner(System.in);

			//get request type
			int request = scanner.nextInt();

			//get request value
			Object value = scanner.nextLine();

			//handle request
			requestHandler(request, value);
		}
	}

	private static void requestHandler(int request, Object value) {
		
		if(request == 3) {
			System.out.print("->\""+list.size()+"\"");
		}
		
		//will accept entries into arrayList as long as they are not null values
		if(!value.equals("") && value != null) {
			if(request == 1) {
				list.add(value);
				System.out.print(list);
			}
			
			if(request == 2) {
				list.removeAll(Collections.singleton(value));
				System.out.print(list);
			}

		}
		
	}

}
